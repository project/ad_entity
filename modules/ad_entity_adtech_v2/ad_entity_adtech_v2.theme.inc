<?php

/**
 * @file
 * Advertising Entity: AdTech v2 Factory theme implementations.
 */

use Drupal\Core\Template\Attribute;
use Drupal\Component\Utility\Html;

/**
 * Preprocess implementation for a default AdTech Factory tag view.
 *
 * @param array &$variables
 *   An array of available variables.
 */
function template_preprocess_adtech_v2_default(array &$variables) {
  /** @var \Drupal\ad_entity\Entity\AdEntityInterface $ad_entity */
  $ad_entity       = $variables['ad_entity'];
  $settings        = $ad_entity->getThirdPartySettings('ad_entity_adtech_v2');
  $config          = \Drupal::config('ad_entity.settings');
  $global_settings = $config->get('adtech_v2_factory');
  $variables['use_theme_breakpoints'] = !empty($global_settings['use_theme_breakpoints']);

  // Generate attributes.
  $id = Html::getUniqueId($ad_entity->id());
  $attributes = new Attribute(['id' => $id]);
  $attributes->addClass('adtech-v2-factory-ad');
  $attributes->addClass('adtech-v2-default-view');

  if (!empty($settings['data_atf_format'])) {
    $attributes->setAttribute('atf-format', $settings['data_atf_format']);
  }
  if (!empty($settings['data_atf_format_size'])) {
    $attributes->setAttribute('atf-formatSize', $settings['data_atf_format_size']);
  }
  if (!empty($settings['data_atf_format_note'])) {
    $attributes->setAttribute('atf-formatNote', $settings['data_atf_format_note']);
  }
  if (!empty($settings['data_atf_custom_slot'])) {
    $attributes->setAttribute('atf-customSlot', $settings['data_atf_custom_slot']);
  }
  if (!empty($global_settings['data_atf_request_type'])) {
    $attributes->setAttribute('atf-requestType', $global_settings['data_atf_request_type']);
  }

  $variables['attributes'] = $attributes;
}
