<?php

namespace Drupal\ad_entity_adtech_v2\Plugin\ad_entity\AdType;

use Drupal\ad_entity\Entity\AdEntityInterface;
use Drupal\ad_entity\Plugin\AdTypeBase;
use Drupal\ad_entity\TargetingCollection;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\node\Entity\NodeType;

/**
 * Type plugin for AdTech Factory advertisement.
 *
 * @AdType(
 *   id = "adtech_v2_factory",
 *   label = "AdTech Factory v2"
 * )
 */
class AdtechType extends AdTypeBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Whether the theme_breakpoints_js module is available or not.
   *
   * @var bool
   */
  protected $themeBreakpointsJsAvailable;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->themeBreakpointsJsAvailable = $container->has('theme_breakpoints_js');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function globalSettingsForm(array $form, FormStateInterface $form_state, Config $config) {
    $element = [];

    $settings = $config->get($this->getPluginDefinition()['id']);

    if ($this->themeBreakpointsJsAvailable) {
      $theme_breakpoints_description = $this->stringTranslation->translate('When this option is enabled, the HTML ad tags are being loaded <em>after</em> it is determined which theme breakpoint is active. When enabled, you can further define whether responsive behavior should be enabled or not (see global frontend settings above). When this option is not enabled, the HTML ad tags will be directly loaded into the DOM.');
    }
    else {
      $theme_breakpoints_description = $this->stringTranslation->translate('This option is not available, because the theme_breakpoints_js module is not installed.');
    }
    $element['use_theme_breakpoints'] = [
      '#type' => 'checkbox',
      '#title' => $this->stringTranslation->translate('Initialize ad slots in respect of theme breakpoints.'),
      '#description' => $theme_breakpoints_description,
      '#default_value' => !empty($settings['use_theme_breakpoints']),
      '#disabled' => !$this->themeBreakpointsJsAvailable,
    ];

    $element['async_tag'] = [
      '#type'          => 'textarea',
      '#title'         => $this->stringTranslation->translate('Async Tag'),
      '#description'   => $this->stringTranslation->translate('(provided by ATF personnel)'),
      '#default_value' => !empty($settings['async_tag']) ? $settings['async_tag'] : '',
    ];

    $element['layout_rules'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->stringTranslation->translate('Layout Rules'),
      '#id'          => 'layout-rules-wrapper',
      '#prefix'      => '<div id="rules-wrapper">',
      '#suffix'      => '</div>',
      '#attached'    => [
        'library' => ['ad_entity_adtech_v2/layout_rules'],
      ],
    ];

    $layout_rules_description = $this->stringTranslation->translate('With layout rules you can define which value the <b>atf-contentType</b> will be assigned to via dataLayer definition.');

    $element['layout_rules']['top'] = [
      '#markup' => '<p>' . $layout_rules_description . '</p><ul class="layout_rules-list">',
    ];

    $rule_count  = !empty($settings['layout_rules']) ? count($settings['layout_rules']) : 1;
    $rule_values = [];
    $form_values = $form_state->getValues();
    if (!empty($form_values['adtech_v2_factory']['layout_rules']['rules'])) {
      $rule_count  = count($form_values['adtech_v2_factory']['layout_rules']['rules']);
      $rule_values = $form_values['adtech_v2_factory']['layout_rules']['rules'];
    }

    if (!empty($settings['layout_rules'])) {
      usort($settings['layout_rules'], [$this, 'sortRulesByWeight']);
    }

    if (!empty($rule_values)) {
      usort($rule_values, [$this, 'sortRulesByWeight']);
    }

    // Loop over and create rules rows.
    for ($i = 0; $i < $rule_count; $i++) {
      $element['layout_rules']['rules'][$i] = [
        '#type'   => 'fieldset',
        '#title'  => $this->stringTranslation->translate('Rule'),
        '#prefix' => '<li><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>',
        '#suffix' => '</li>',
      ];

      $value = NULL;
      if (isset($rule_values[$i])) {
        $value = $rule_values[$i];
      }
      elseif (isset($settings['layout_rules'][$i])) {
        $value = $settings['layout_rules'][$i];
      }

      $element['layout_rules']['rules'][$i]['weight'] = [
        '#type'          => 'textfield',
        '#default_value' => $value['weight'] ?? $i + 1,
        '#attributes'    => ['class' => ['rules_weight-wrapper']],
      ];

      $element['layout_rules']['rules'][$i]['rule_type'] = [
        '#type'          => 'select',
        '#title'         => $this->stringTranslation->translate('Type'),
        '#options'       => [
          'none'      => $this->stringTranslation->translate('Select a rule type'),
          'default'   => $this->stringTranslation->translate('Default Layout (Fallback)'),
          'node_type' => $this->stringTranslation->translate('Node Type'),
          'term_type' => $this->stringTranslation->translate('Term Type (Vocabulary)'),
          'url_regex' => $this->stringTranslation->translate('Url Regex Match'),
        ],
        '#default_value' => $value['rule_type'] ?? NULL,
        '#ajax'          => [
          'callback' => [static::class, 'reloadRules'],
          'event'    => 'change',
          'wrapper'  => 'rules-wrapper',
        ],
      ];

      if (isset($value['rule_type'])) {
        $this->ruleRowHandler($value, $element['layout_rules']['rules'][$i]);
      }
    }

    $element['layout_rules']['bottom'] = [
      '#markup' => '</ul>',
    ];

    $element['layout_rules']['buttons']['add'] = [
      '#type'    => 'submit',
      '#value'   => $this->stringTranslation->translate('Add rule'),
      '#el_data' => ['field_name' => 'layout_rules', 'element' => 'rules'],
      '#submit'  => ['\Drupal\ad_entity_adtech_v2\Plugin\ad_entity\AdType\AdtechType::addMore'],
      '#ajax'    => [
        'callback' => [static::class, 'reloadRules'],
        'wrapper'  => 'rules-wrapper',
      ],
    ];

    $element['layout_rules']['buttons']['remove'] = [
      '#type'    => 'submit',
      '#value'   => $this->stringTranslation->translate('Remove last rule'),
      '#el_data' => ['field_name' => 'layout_rules', 'element' => 'rules'],
      '#submit'  => ['\Drupal\ad_entity_adtech_v2\Plugin\ad_entity\AdType\AdtechType::removeLast'],
      '#ajax'    => [
        'callback' => [static::class, 'reloadRules'],
        'wrapper'  => 'rules-wrapper',
      ],
    ];

    $customs_slots_num = !empty($settings['custom_slot']) ? count($settings['custom_slot']) : 1;
    $slot_values       = [];
    $form_values       = $form_state->getValues();
    if (isset($form_values['adtech_v2_factory']['custom_slot']['slots'])) {
      $customs_slots_num = count($form_values['adtech_v2_factory']['custom_slot']['slots']);
      $slot_values       = $form_values['adtech_v2_factory']['custom_slot']['slots'];
    }

    // Custom slots.
    $element['custom_slot'] = [
      '#type'        => 'fieldset',
      '#prefix'      => '<div id="slots-wrapper">',
      '#suffix'      => '</div>',
      '#title'       => $this->stringTranslation->translate('Custom slots'),
      '#description' => $this->stringTranslation->translate('Global definition of custom slots.'),
      '#id'          => 'fieldset-custom_slot-wrapper',
    ];

    for ($i = 0; $i < $customs_slots_num; $i++) {
      $value = NULL;
      if (isset($slot_values[$i])) {
        $value = $slot_values[$i];
      }
      elseif (isset($settings['custom_slot'][$i])) {
        $value = $settings['custom_slot'][$i];
      }

      $element['custom_slot']['slots'][$i]['name'] = [
        '#type'          => 'textfield',
        '#title'         => $this->stringTranslation->translate('Name'),
        '#description'   => $this->stringTranslation->translate('Characters [\'ä\', \'Ä\', \'ü\', \'Ü\', \'ö\', \'Ö\', \'ß\', \'&\', \' \'] are not allowed, and they will be replaced.'),
        '#default_value' => $value['name'] ?? '',
      ];
    }

    $element['custom_slot']['buttons']['add'] = [
      '#type'    => 'submit',
      '#value'   => $this->stringTranslation->translate('Add slot'),
      '#el_data' => ['field_name' => 'custom_slot', 'element' => 'slots'],
      '#submit'  => ['\Drupal\ad_entity_adtech_v2\Plugin\ad_entity\AdType\AdtechType::addMore'],
      '#ajax'    => [
        'callback' => [static::class, 'reloadSlots'],
        'wrapper'  => 'slots-wrapper',
      ],
    ];

    $element['custom_slot']['buttons']['remove'] = [
      '#type'    => 'submit',
      '#value'   => $this->stringTranslation->translate('Remove last slot'),
      '#el_data' => ['field_name' => 'custom_slot', 'element' => 'slots'],
      '#submit'  => ['\Drupal\ad_entity_adtech_v2\Plugin\ad_entity\AdType\AdtechType::removeLast'],
      '#ajax'    => [
        'callback' => [static::class, 'reloadSlots'],
        'wrapper'  => 'slots-wrapper',
      ],
    ];

    $element['data_atf_use_lazy_load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->stringTranslation->translate('Lazy Loading'),
      '#description'   => $this->stringTranslation->translate('Function provided by Google Publisher Tag.'),
      '#default_value' => $settings['data_atf_use_lazy_load'] ?? NULL,
    ];

    $element['data_atf_request_type'] = [
      '#type'          => 'select',
      '#title'         => $this->stringTranslation->translate('Value for the data-atf-requestType attribute on the ad tag'),
      '#default_value' => !empty($settings['data_atf_request_type']) ? $settings['data_atf_request_type'] : 'single_request',
      '#description'   => $this->stringTranslation->translate('For Lazy Loading, <strong>Multi Request Architecture is desired by some publishers.</strong>'),
      '#field_prefix'  => 'data-atf-requestType="',
      '#field_suffix'  => '"',
      '#options'       => [
        'single_request' => $this->stringTranslation->translate('Single request'),
        'multi_request'  => $this->stringTranslation->translate('Multi request'),
      ],
    ];

    $targeting                 = !empty($settings['page_targeting']) ?
      new TargetingCollection($settings['page_targeting']) : NULL;
    $element['page_targeting'] = [
      '#type'          => 'textfield',
      '#title'         => $this->stringTranslation->translate('Default page targeting'),
      '#description'   => $this->stringTranslation->translate('Default pairs of key-values for targeting on the page. Example: <strong>pos: top, category: value1, category: value2, ...</strong>'),
      '#default_value' => !empty($targeting) ? $targeting->toUserOutput() : '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function globalSettingsSubmit(array &$form, FormStateInterface $form_state, Config $config) {
    $id     = $this->getPluginDefinition()['id'];
    $values = $form_state->getValue($id);

    $config->set($id . '.use_theme_breakpoints', !empty($values['use_theme_breakpoints']));
    $request_type = $values['data_atf_request_type'];
    if ('single_request' === $request_type) {
      $config->set($id . '.data_atf_request_type', NULL);
    }
    $lazy_loading = $values['data_atf_use_lazy_load'];
    if (0 === $lazy_loading) {
      $config->set($id . '.data_atf_use_lazy_load', NULL);
    }
    if (isset($values['layout_rules']['rules'])) {
      foreach ($values['layout_rules']['rules'] as $key => $rule) {
        if (empty($rule['layout'])) {
          unset($values['layout_rules']['rules'][$key]);
        }
        else {
          $values['layout_rules']['rules'][$key]['layout'] = self::sanitizeString($rule['layout']);
        }
        if (isset($rule['weight'])) {
          $values['layout_rules']['rules'][$key]['weight'] = (int) $rule['weight'];
        }
        // For selected entity types, only include the selected ones.
        if (!empty($rule['type'])) {
          $types_selected = [];
          foreach ($rule['type'] as $type_key => $type_value) {
            if ($type_value && $type_value === $type_key) {
              $types_selected[$type_key] = $type_value;
            }
          }
          $values['layout_rules']['rules'][$key]['type'] = $types_selected;
        }
      }
      // Re-sort the rules by given weight.
      if (!empty($values['layout_rules']['rules'])) {
        usort($values['layout_rules']['rules'], function ($a, $b) {
          if (isset($a['weight'], $b['weight'])) {
            return (int) $a['weight'] - (int) $b['weight'];
          }
          return 0;
        });
      }
      $config->set($id . '.layout_rules', array_values($values['layout_rules']['rules']));
    }
    if (isset($values['custom_slot']) && isset($values['custom_slot']['slots'])) {
      foreach ($values['custom_slot']['slots'] as $key => $slot) {
        if (empty($slot['name'])) {
          unset($values['custom_slot']['slots'][$key]);
        }
        else {
          $values['custom_slot']['slots'][$key]['name'] = self::sanitizeString($slot['name']);
        }
      }
      $config->set($id . '.custom_slot', array_values($values['custom_slot']['slots']));
    }

    if (!empty($values['page_targeting'])) {
      // Convert the targeting to a JSON-encoded string.
      $targeting = new TargetingCollection();
      $targeting->collectFromUserInput($values['page_targeting']);
      $config->set($id . '.page_targeting', $targeting->toJson());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function entityConfigForm(array $form, FormStateInterface $form_state, AdEntityInterface $ad_entity) {
    $element  = [];
    $settings = $ad_entity->getThirdPartySettings($this->getPluginDefinition()['provider']);

    $element['#attached'] = [
      'library' => ['ad_entity_adtech_v2/form_validation'],
    ];

    $element['data_atf_format'] = [
      '#type'          => 'select',
      '#title'         => $this->stringTranslation->translate('Value for the data-atf-format attribute on the ad tag'),
      '#default_value' => !empty($settings['data_atf_format']) ? $settings['data_atf_format'] : '',
      '#description'   => $this->stringTranslation->translate('Ad slot type.'),
      '#field_prefix'  => 'data-atf-format="',
      '#field_suffix'  => '"',
      '#options'       => [
        'top'      => $this->stringTranslation->translate('top'),
        'vertical' => $this->stringTranslation->translate('vertical'),
        'content'  => $this->stringTranslation->translate('content'),
        'footer'   => $this->stringTranslation->translate('footer'),
        'custom'   => $this->stringTranslation->translate('custom'),
      ],
      '#required'      => TRUE,
    ];

    $element['data_atf_format_size'] = [
      '#type'          => 'textfield',
      '#title'         => $this->stringTranslation->translate('Value for the data-atf-formatSize attribute on the ad tag'),
      '#default_value' => !empty($settings['data_atf_format_size']) ? $settings['data_atf_format_size'] : '',
      '#description'   => $this->stringTranslation->translate('Maximum size from the size bucket for selected atf-format, <strong>defined as ‘WIDTHxHEIGHT’ (e.g. 1200x800)</strong>.'),
      '#field_prefix'  => 'data-atf-formatSize="',
      '#field_suffix'  => '"',
      '#size'          => 15,
      '#states'        => [
        'required' => [
          ':input[name="data_atf_format"]' => ['!value' => 'custom'],
        ],
      ],
    ];

    $element['data_atf_custom_slot'] = [
      '#type'          => 'select',
      '#title'         => $this->stringTranslation->translate('Value for the data-atf-customSlot attribute on the ad tag'),
      '#default_value' => !empty($settings['data_atf_custom_slot']) ? $settings['data_atf_custom_slot'] : 'default',
      '#description'   => $this->stringTranslation->translate('<strong>Custom script slots</strong> are defined in global settings.'),
      '#field_prefix'  => 'data-atf-customSlot="',
      '#field_suffix'  => '"',
      '#options'       => ['default' => $this->stringTranslation->translate('Default')] + $this->slotsList(),
    ];

    $element['data_atf_format_note'] = [
      '#type'          => 'select',
      '#title'         => $this->stringTranslation->translate('Value for the data-atf-formatNote attribute on the ad tag'),
      '#default_value' => !empty($settings['data_atf_format_note']) ? $settings['data_atf_format_note'] : 'standard',
      '#description'   => $this->stringTranslation->translate('Include special sizes (e.g. 1000x1, 300x251...)'),
      '#field_prefix'  => 'data-atf-formatNote="',
      '#field_suffix'  => '"',
      '#options'       => [
        'standard' => $this->stringTranslation->translate('standard'),
        'special'  => $this->stringTranslation->translate('special'),
        'native'  => $this->stringTranslation->translate('native'),
        'special,native'  => $this->stringTranslation->translate('special,native'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function entityConfigSubmit(array &$form, FormStateInterface $form_state, AdEntityInterface $ad_entity) {
    $provider = $this->getPluginDefinition()['provider'];
    $values   = $form_state->getValue(['third_party_settings', $provider]);

    $format = $values['data_atf_format'];
    if ('custom' === $format) {
      $ad_entity->setThirdPartySetting($provider, 'data_atf_format_size', NULL);
    }

    $custom_slot = $values['data_atf_custom_slot'] ?? NULL;
    if ('custom' !== $custom_slot) {
      $ad_entity->setThirdPartySetting($provider, 'data_atf_custom_slot', NULL);
    }

    $format_note = $values['data_atf_format_note'] ?? NULL;
    if ('standard' === $format_note) {
      $ad_entity->setThirdPartySetting($provider, 'data_atf_format_note', NULL);
    }
  }

  /**
   * Returns a default targeting collection.
   *
   * @return \Drupal\ad_entity\TargetingCollection
   *   The default targeting collection.
   */
  protected function defaultTargeting() {
    $info = [];
    if ($config = $this->configFactory->get('system.site')) {
      $info['website'] = $config->get('name');
    }
    return new TargetingCollection($info);
  }

  /**
   * Returns a globally defined custom slots.
   *
   * @return array
   *   Slots.
   */
  protected function slotsList() {
    $slots = [];
    if ($config = $this->configFactory->get('ad_entity.settings')) {
      if ($global_settings = $config->get('adtech_v2_factory')) {
        if ($custom_slot = $global_settings['custom_slot']) {
          $all_slots = array_column($custom_slot['slots'], 'name') ?? array_column($custom_slot, 'name');
          foreach ($all_slots as $slot) {
            $machine_name                     = str_replace(' ', '_', $slot);
            $slots[strtolower($machine_name)] = $this->stringTranslation->translate($slot);
          }
        }
      }
    }

    return $slots;
  }

  /**
   * Ajax callback for add/remove rule, returns only the inner form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return array|mixed
   */
  public static function reloadRules(array &$form, FormStateInterface $formState) {
    return $form['adtech_v2_factory']['layout_rules'] ?? [];
  }

  /**
   * Ajax callback for add/remove slot, returns only the inner form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return array|mixed
   */
  public static function reloadSlots(array &$form, FormStateInterface $formState) {
    return $form['adtech_v2_factory']['custom_slot'] ?? [];
  }

  /**
   * Add a rule/slot to the form state values.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public static function addMore(array &$form, FormStateInterface $formState) {
    $values = $formState->getValues();

    // Get triggered element data.
    $triggered_element = $formState->getTriggeringElement();
    $data              = $triggered_element['#el_data'];
    $field_name        = $data['field_name'];
    $element           = $data['element'];

    if (isset($values['adtech_v2_factory'])) {
      $rules                          = &$values['adtech_v2_factory'];
      $rules[$field_name][$element][] = [];
      $formState->setValues($values);
      $formState->setRebuild();
    }
  }

  /**
   * Remove a rule/slot row from the form state values.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public static function removeLast(array &$form, FormStateInterface $formState) {
    $values = $formState->getValues();

    // Get triggered element data.
    $triggered_element = $formState->getTriggeringElement();
    $data              = $triggered_element['#el_data'];
    $field_name        = $data['field_name'];
    $element           = $data['element'];

    if (isset($values['adtech_v2_factory'])) {
      $fields = &$values['adtech_v2_factory'];
      if (count($fields[$field_name][$element]) > 1) {
        unset($fields[$field_name][$element][count($fields[$field_name][$element]) - 1]);
      }
      $formState->setValues($values);
      $formState->setRebuild();
    }
  }

  /**
   * Sort rules by weight (usort).
   *
   * @param mixed $a
   *   Parameter a.
   * @param mixed $b
   *   Parameter b.
   *
   * @return int
   *   The calculated weight sort.
   */
  private function sortRulesByWeight($a, $b) {
    $a_weight = intval($a['weight'] ?? 99999);
    $b_weight = intval($b['weight'] ?? 99999);
    return $a_weight - $b_weight;
  }

  /**
   * Add form input.
   *
   * @param array $value
   *   Values.
   * @param array $rule_element
   *   Rule row element.
   */
  private function ruleRowHandler(array $value, array &$rule_element) {
    $rule_type                = $value['rule_type'];
    $layout_input_description = $this->stringTranslation->translate('Use one of the values: <strong>homepage, channel, article or gallery.</strong>');
    switch ($rule_type) {
      case 'url_regex':
        $rule_element['regex'] = [
          '#type'          => 'textfield',
          '#title'         => $this->stringTranslation->translate('Regex'),
          '#description'   => $this->stringTranslation->translate('WARNING: No validation is done! Incorrect values can cause JS errors! Starting and ending slash will be added automatically.'),
          '#default_value' => $value['regex'] ?? NULL,
          '#weight'        => 10,
        ];
        break;

      case 'node_type':
        $node_types = NodeType::loadMultiple();
        $options    = [];
        foreach ($node_types as $node_type) {
          $options[$node_type->id()] = $node_type->label();
        }

        $rule_element['type'] = [
          '#type'          => 'checkboxes',
          '#title'         => $this->stringTranslation->translate('Node Type'),
          '#description'   => $this->stringTranslation->translate('Select the node types to apply this rule on.'),
          '#default_value' => $value['type'] ?? [],
          '#options'       => $options,
          '#weight'        => 10,
        ];
        break;

      case 'term_type':
        $vocabularies = Vocabulary::loadMultiple();
        $options      = [];
        foreach ($vocabularies as $vocabulary) {
          $options[$vocabulary->id()] = $vocabulary->label();
        }

        $rule_element['type'] = [
          '#type'          => 'checkboxes',
          '#title'         => $this->stringTranslation->translate('Term Type (Vocabulary)'),
          '#description'   => $this->stringTranslation->translate('Select the term types to apply this rule on.'),
          '#default_value' => $value['type'] ?? [],
          '#options'       => $options,
          '#weight'        => 10,
        ];
        break;

      case 'default':
        $layout_input_description = $this->stringTranslation->translate('<strong>Fallback layout value. </strong> Use one of the values: <strong>homepage, channel, article or gallery.</strong>');
        break;
    }

    $rule_element['layout'] = [
      '#type'          => 'textfield',
      '#title'         => $this->stringTranslation->translate('Layout (rule output)'),
      '#description'   => $layout_input_description,
      '#default_value' => $value['layout'] ?? NULL,
      '#weight'        => 10,
    ];
  }

  /**
   * Removes unwanted chars from string.
   *
   * @param string $string
   *   Value or key string to be sanitized.
   * @param string $type
   *   Indicates whether the sting defines a key or a value, as there apply
   *   slightly different sanitation rules.
   *
   * @return string
   *   The sanitized string.
   */
  private static function sanitizeString($string, $type = 'value') {
    $string = trim(strtolower($string));
    $string = str_replace(['ä', 'Ä', 'ü', 'Ü', 'ö', 'Ö', 'ß', '&', ' '], [
      'ae',
      'ae',
      'ue',
      'ue',
      'oe',
      'oe',
      'ss',
      '-',
      '_',
    ], $string);
    // Remove remaining unsafe characters.
    $string = preg_replace('/[^0-9a-z_-]/', '', $string);
    // Limit string length (keys to 20 and values to 40 characters).
    $string = substr($string, 0, ('key' == $type) ? 20 : 40);
    return $string;
  }

}
