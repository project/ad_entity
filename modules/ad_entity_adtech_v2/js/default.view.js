/**
 * @file
 * JS View handler implementation for ads which are using the
 *     'adtech_v2_default' view plugin.
 */

(function (ad_entity) {

  ad_entity.viewHandlers = ad_entity.viewHandlers || {};

  ad_entity.viewHandlers.adtech_v2_default = {
    initialize: function (containers, context, settings) {
      var container;
      var buffer = {};

      for (buffer.containerId in containers) {
        if (containers.hasOwnProperty(buffer.containerId)) {
          container = containers[buffer.containerId];
          buffer.applicant = container.el.querySelector('.slot-applicant');
          if (buffer.applicant) {
            buffer.slotHtml = JSON.parse(buffer.applicant.getAttribute('data-slot'));
            if (buffer.slotHtml) {
              ad_entity.helpers.trigger(window, 'atf:BeforeSlotLoad', false, true, { container: container, slotHtml: buffer.slotHtml });
              // Once the slot element is in the DOM, it will be automatically
              // recognized and loaded by the ATF PPTM/SDK.
              container.el.innerHTML = buffer.slotHtml;
            }
          }
          buffer.ad_el = container.el.querySelector('.adtech-v2-factory-ad');
          if (buffer.ad_el === null) {
            continue;
          }

          this.numberOfAds++;
          container.ad_tag = {
            el: buffer.ad_el,
            data: function (key, value) {
              return ad_entity.helpers.metadata(this.el, this, key, value);
            }
          };
        }
      }
    },
    detach: function (containers, context, settings) {},
    numberOfAds: 0,
  };

}(window.adEntity));
