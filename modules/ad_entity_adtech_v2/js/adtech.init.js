/**
 * @file
 * JS implementation for initializing atf-* dataLayer variables.
 */
(function (window, adEntity, settings) {

  adEntity.adTech = adEntity.adTech || {};
  window.dataLayer = window.dataLayer || [];
  window.googletag = window.googletag || {};
  window.googletag.cmd = window.googletag.cmd || [];

  /**
   * Initialize function.
   *
   * @param settings
   */
  adEntity.adTech.init = adEntity.adTech.init || function (settings) {
    var buffer = {};
    if (!settings.hasOwnProperty('ad_entity_adtech_v2')) {
      return;
    }
    buffer = {use_lazy_load: null, content_type: null};
    buffer.settings = settings.ad_entity_adtech_v2;
    if (!buffer.settings.hasOwnProperty('page_targeting')) {
      buffer.settings.page_targeting = {};
    }

    if (buffer.settings.global_settings) {
      if (!this.globalPageTargeting && buffer.settings.global_settings.page_targeting) {
        this.globalPageTargeting = JSON.parse(buffer.settings.global_settings.page_targeting);
      }

      if (buffer.settings.global_settings.data_atf_use_lazy_load) {
        buffer.use_lazy_load = true;
      }

      if (buffer.settings.global_settings.layout_rules) {
        buffer.layout_rules = buffer.settings.global_settings.layout_rules;
        if (Array.isArray(buffer.layout_rules)) {
          buffer.layout_rules.sort(this.weightComparator);
          buffer.content_type = this.getLayout(buffer.layout_rules, buffer.settings.page_targeting);
        }
      }
    }
    // In case that channel value is empty on page targeting,
    // we use the one used in global entity settings.
    buffer.channel = this.getChannel(buffer.settings.page_targeting);
    if (!buffer.channel && this.globalPageTargeting) {
      buffer.channel = this.getChannel(this.globalPageTargeting);
    }

    window.googletag.cmd.push(function () {
      window.googletag.pubads().addEventListener('slotRenderEnded', function (event) {
        var eb = {id: event.slot.getSlotElementId()};
        var helpers = adEntity.helpers || null;
        if (!helpers || !eb.id || !adEntity.adContainers) {
          return;
        }
        eb.slot_el = window.document.getElementById(eb.id);
        if (!eb.slot_el) {
          return;
        }
        eb.i = 0;
        eb.el = eb.slot_el.parentNode;
        while (eb.i < 3 && eb.el && !eb.container) {
          for (eb.containerId in adEntity.adContainers) {
            if (adEntity.adContainers.hasOwnProperty(eb.containerId)) {
              eb.container = adEntity.adContainers[eb.containerId];
              if (eb.container.ad_tag && eb.container.ad_tag.el && eb.container.ad_tag.el === eb.el) {
                break;
              }
            }
            eb.container = null;
          }
          eb.el = eb.el.parentNode;
          eb.i++;
        }
        if (!eb.container) {
          return;
        }
        eb.el = eb.container.el;
        helpers.removeClass(eb.el, 'not-initialized');
        helpers.addClass(eb.el, 'initialized');
        eb.container.data('initialized', true);
        if (event.isEmpty === true) {
          helpers.addClass(eb.el, 'empty');
          helpers.removeClass(eb.el, 'not-empty');
        }
        else {
          helpers.addClass(eb.el, 'not-empty');
          helpers.removeClass(eb.el, 'empty');
        }
        helpers.trigger(eb.el, 'adEntity:initialized', true, true, { container: eb.container });
      });
    });

    this.insertIntoDataLayer('atf-useLazyLoad', buffer.use_lazy_load);
    this.insertIntoDataLayer('atf-contentType', buffer.content_type);
    this.insertIntoDataLayer('atf-channel', buffer.channel);
    this.insertIntoDataLayer('atf-targeting', buffer.settings.page_targeting);
  }.bind(adEntity.adTech);

  /**
   * Inserts data into dataLayer.
   *
   * @param tag
   * @param value
   *
   */
  adEntity.adTech.insertIntoDataLayer = function (tag, value) {
    var buffer = {};
    if (typeof value === 'undefined' || '' === value || null === value) {
      return;
    }
    buffer[tag] = value;
    window.dataLayer.push(buffer);
  }

  /**
   * Compare function for layout rules (weight property).
   *
   * @param a
   * @param b
   * @returns {number}
   */
  adEntity.adTech.weightComparator = function (a, b) {
    return (a.weight > b.weight) ? 1 : ((b.weight > a.weight) ? -1 : 0);
  };

  /**
   * Get the current page channel.
   *
   * @param containerTargetingData
   *
   * @returns {string}
   */
  adEntity.adTech.getChannel = function (containerTargetingData) {
    var channel = null;
    if (containerTargetingData.hasOwnProperty('channel')) {
      channel = containerTargetingData.channel;
    }
    // Currently we can set only one value in atf-channel,
    // so in case of the array we set last one.
    if (Array.isArray(channel)) {
      channel = channel[channel.length - 1];
    }
    return channel;
  };

  /**
   * Get the layout string value.
   *
   * @param layout_rules
   * @param containerTargetingData
   *
   * @returns {string|null}
   */
  adEntity.adTech.getLayout = function (layout_rules, containerTargetingData) {
    var buffer = {layout: null, i: 0};

    for (buffer.i = 0; buffer.i < layout_rules.length; buffer.i++) {
      buffer.layout = this.processRule(layout_rules[buffer.i], containerTargetingData);
      if (buffer.layout && buffer.layout.length) {
        return buffer.layout;
      }
    }

    return null;
  }.bind(adEntity.adTech);

  /**
   * Process the provided rule.
   *
   * @param rule
   * @param containerTargetingData
   * @returns {*}
   */
  adEntity.adTech.processRule = function (rule, containerTargetingData) {
    if (!rule.hasOwnProperty('rule_type')) {
      return null;
    }

    switch (rule.rule_type) {
      case 'default':
        return this.processLayoutRuleDefault(rule);

      case 'url_regex':
        return this.processLayoutRuleUrlRegex(rule);

      case 'node_type':
        return this.processLayoutRuleEntityType(rule, containerTargetingData, 'node');

      case 'term_type':
        return this.processLayoutRuleEntityType(rule, containerTargetingData, 'taxonomy_term');

    }
  }.bind(adEntity.adTech);

  /**
   * Process the "default" value rule.
   *
   * @param ruleConfig
   * @returns {*}
   */
  adEntity.adTech.processLayoutRuleDefault = function (ruleConfig) {
    return ruleConfig.hasOwnProperty('layout') && '' !== ruleConfig.layout ? ruleConfig.layout : null;
  };

  /**
   * Process the url Regex rule.
   *
   * @param ruleConfig
   * @returns {*}
   */
  adEntity.adTech.processLayoutRuleUrlRegex = function (ruleConfig) {
    if (ruleConfig.hasOwnProperty('regex')) {
      var currentUrl = document.location.href;
      try {
        var regex = new RegExp(ruleConfig.regex, 'g');
        return regex.exec(currentUrl) ? ruleConfig.layout : null;
      }
      catch (e) {
        console.log('Error with regex or incorrect regex defined in a AdTech v2 layout rule!');
      }
      return null;

    }
    return null;
  };

  /**
   * Process the entity type rule.
   *
   * @param ruleConfig
   * @param containerTargetingData
   * @param entityType
   * @returns {*}
   */
  adEntity.adTech.processLayoutRuleEntityType = function (ruleConfig, containerTargetingData, entityType) {
    var entityTypeBundle = this.getEntityTypeBundle(containerTargetingData, entityType);
    if (null !== entityTypeBundle && ruleConfig.hasOwnProperty('type')) {
      if (ruleConfig.type.hasOwnProperty(entityTypeBundle) && ruleConfig.type[entityTypeBundle] !== 0) {
        return this.replaceEntityTypeRuleTokens(ruleConfig, entityType, entityTypeBundle);
      }
    }
    return null;
  }.bind(adEntity.adTech);

  /**
   * Get the bundle type of the provided entity type, if it is of the correct
   * type.
   *
   * @param containerTargetingData
   * @param entityType
   * @returns {*}
   */
  adEntity.adTech.getEntityTypeBundle = function (containerTargetingData, entityType) {
    var entityTypeBundle = null;
    var entityTypeValue = containerTargetingData.hasOwnProperty('entitytype') ? containerTargetingData.entitytype : false;
    if (false !== entityTypeValue) {
      entityTypeBundle = entityTypeValue.indexOf(entityType + '/') !== -1 ? entityTypeValue.replace(entityType + '/', '') : entityTypeBundle;
    }
    return entityTypeBundle;
  };

  /**
   * Replace the tokens on the
   *
   * @param ruleConfig
   * @param entityType
   * @param entityTypeBundle
   * @returns {*}
   */
  adEntity.adTech.replaceEntityTypeRuleTokens = function (ruleConfig, entityType, entityTypeBundle) {
    var layout = ruleConfig.layout;

    switch (ruleConfig.rule_type) {
      case 'node_type': {
        layout = layout.replace(/\[type\]/g, entityType);
        layout = layout.replace(/\[bundle\]/g, entityTypeBundle);
        break;

      }
      case 'term_type': {
        layout = layout.replace(/\[type\]/g, entityType);
        break;

      }
    }
    return layout;
  };

  // Add data to dataLayer.
  adEntity.adTech.init(settings);

}(window, window.adEntity, window.drupalSettings));
