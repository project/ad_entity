(function ($, Drupal) {

  Drupal.behaviors.formValidation = {
    attach: function attach(context, settings) {
      let $atfFormat = $('select[name="third_party_settings[ad_entity_adtech_v2][data_atf_format]"]');

      let $fields = {
        $atfFormatSize: $('input[name="third_party_settings[ad_entity_adtech_v2][data_atf_format_size]"]'),
        $atfCustomSlot: $('select[name="third_party_settings[ad_entity_adtech_v2][data_atf_custom_slot]"]'),
        $atfFormatNote: $('select[name="third_party_settings[ad_entity_adtech_v2][data_atf_format_note]"]'),
      };

      let formValidation = new FormValidation();
      let atfFormatValue = $atfFormat.val();

      // Validate form base on value of atf format.
      formValidation.atfFormat($fields, atfFormatValue);

      $atfFormat.on('change',
          function () {
            formValidation.atfFormat($fields, $(this).val());
          });
    },
  };

}(jQuery, Drupal));

/**
 * Validation.
 */
function FormValidation() {

  this.atfFormat = function ($fields, value) {

    // Custom slot defined only for custom.
    this.disable([$fields.$atfCustomSlot]);
    this.hide([$fields.$atfCustomSlot]);
    this.clearAll($fields);

    switch (value) {
      case 'footer':
        this.clearAll($fields);
        this.setValues({
          '4x4': $fields.$atfFormatSize,
          'special': $fields.$atfFormatNote
        });
        this.disable([$fields.$atfFormatSize, $fields.$atfCustomSlot, $fields.$atfFormatNote]);
        this.hide([$fields.$atfCustomSlot]);
        break;

      case 'custom':
        this.clearAll($fields);
        this.disable([$fields.$atfCustomSlot], false);
        this.hide([$fields.$atfCustomSlot], false);
        this.disable([$fields.$atfFormatSize]);
        this.hide([$fields.$atfFormatSize]);
        break;
    }
  };

  /**
   * Set default.
   *
   * @param fields
   *   Array of field selectors.
   */
  this.clearAll =
      function (fields) {
        this.disable([fields.$atfFormatSize, fields.$atfFormatNote], false);
        this.hide([fields.$atfFormatSize, fields.$atfFormatNote], false);
      };

  /**
   * Disable/enable fields.
   *
   * @param fields
   *   Array of field selectors.
   * @param isDisable
   *   Boolean disable.
   */
  this.disable =
      function (fields, isDisable = true) {
        jQuery.each(fields, function (index, value) {
          // Disable is not readable.
          jQuery(value).attr('readonly', isDisable);
        });
      };

  /**
   * Hide/show fields.
   *
   * @param fields
   *   Array of field selectors.
   * @param isHide
   *   Boolean hide.
   */
  this.hide =
      function (fields, isHide = true) {
        jQuery.each(fields, function (index, value) {
          // Remove required from hidden fields - valid since only applies to
          // atf-customSlot.
          jQuery(value).attr('required', !isHide);
          jQuery(value).parent().parent().attr('hidden', isHide);
        });
      };

  /**
   * Set field values.
   *
   * @param fields
   *   Array of field selectors.
   */
  this.setValues =
      function (fields) {
        jQuery.each(fields, function (value, field) {
          jQuery(field).val(value);
        });
      };

}
