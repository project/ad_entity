(function ($, Drupal, Sortable) {

  Drupal.behaviors.adtechv2LayoutRules = {
    sel: '#layout-rules-wrapper ul.layout_rules-list',
    attach: function attach(context, settings) {
      var $ul = $(this.sel);
      var self = this;
      Sortable.create($ul[0], {
        onEnd: function (event) {
          self.setWeightValues.call(self);
        }
      });
      this.hideWeightFields();
    },
    /**
     * Re-set the row weight values.
     */
    setWeightValues: function () {
      var counter = 1;
      $(this.sel + ' .rules_weight-wrapper').each(function () {
        $(this).val(counter++);
      });
    },
    /**
     * Hides the weight fields.
     */
    hideWeightFields: function () {
      $(this.sel + ' .rules_weight-wrapper').each(function () {
        $(this).closest('.form-item').css('display', 'none');
      });
    }
  };

})(jQuery, Drupal, Sortable);
