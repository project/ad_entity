<?php

use Drupal\ad_entity\Entity\AdEntity;
use Drupal\Core\Config\Entity\ConfigEntityUpdater;

/**
 * Fixes ad_entity_dfp third party settings.
 */
function ad_entity_dfp_post_update_fix_actions_config(&$sandbox = NULL) {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'ad_entity', function (AdEntity $ad_entity) {
    $dfp_settings = $ad_entity->getThirdPartySettings('ad_entity_dfp');
    if (!empty($dfp_settings) && isset($dfp_settings['amp']['rtc_config']['vendors']['actions'])) {
      unset($dfp_settings['amp']['rtc_config']['vendors']['actions']);
      $ad_entity->setThirdPartySetting('ad_entity_dfp', 'amp', $dfp_settings['amp']);
      return TRUE;
    }
    return FALSE;
  });
}
