/**
 * @file
 * Initializes Google Publisher Tag (GPT) service provider.
 */

(function (window) {

  window.googletag = window.googletag || {};
  window.googletag.cmd = window.googletag.cmd || [];

  window.googletag.cmd.push(function () {
    var gpt = window.googletag;
    var p13n = false;
    if (window.adEntity && typeof window.adEntity.usePersonalization === 'function') {
      p13n = window.adEntity.usePersonalization();
    }
    gpt.pubads().enableSingleRequest(true);
    gpt.pubads().disableInitialLoad();
    gpt.pubads().collapseEmptyDivs();
    if (p13n === true) {
      gpt.pubads().setRequestNonPersonalizedAds(0);
    }
    else if (p13n === false) {
      gpt.pubads().setRequestNonPersonalizedAds(1);
    }
    gpt.enableServices();
  });

}(window));
